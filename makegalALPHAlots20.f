      PROGRAM makegalALPHAlots20   !! lots2
C======================================================================
C     Kochanek's alphapot
C
C     As in all other makegal.f codes, increasing ispm will not
C     improve precision of the estimated caustic lines, but will increase
C     the number of points along the caustic. The precision is set by
C     the fact that the code linearly interpolates between grid locations. 

C     SUBR interlpot is not used (compared to previous makegal.f codes)
C     because potential and other quantities are calculated analytically
C     at any point in the lens plane.
C
C     q2<1 elongates the mass distribution along the x-direction
C     q2>1 elongates the mass distribution along the y-direction
C
C     K2>0 elongates the mass distribution along PA~135 degrees
C     K2<0 elongates the mass distribution along PA~ 45 degrees
C
C     Here, images distances and angles are w.r.t. highest density pixel,
C     ixmax,iymax, NOT anph,anph
C
C     sc: changes the radial scaling of the lens while keeping kappa,magnif at R_Einstein the same
C======================================================================
      IMPLICIT REAL*8 (a-z)
      INTEGER iix,iiy,iax,iay,ixoff,iyoff,ixnew,iynew,ixext,iyext
      INTEGER ir2,iz,ixs,iys,nminus,mminus,FLAG,icount,m,ord(1:4),ik,in
      INTEGER jim,ieigen1,ieigen2,ibis12,ibis34,iquad12,iquad34,inmax
      INTEGER idum,hiim,ixe,iye,ispm,ixmax,iymax,jl,inc,FLAGa,FLAGk
      PARAMETER (ispm=10,inmax=100000)   
      INCLUDE 'commonALPHA.file'
      DIMENSION gam1(0:np,0:np),gam2(0:np,0:np),kap(0:np,0:np)
     &         ,delxx(0:np-1,0:np-1),delyy(0:np-1,0:np-1)
     &         ,lpot(0:np,0:np)
     &         ,redxpt(1:2),redypt(1:2),bluxpt(1:2),bluypt(1:2)
     &         ,subeigen1(1:ispm,1:ispm),subeigen2(1:ispm,1:ispm)
     &         ,subgradx(1:ispm,1:ispm),subgrady(1:ispm,1:ispm)
      EXTERNAL ran3,intereigen1,intereigen2,intergradx,intergrady
     &         ,flip,alplens,setscale,adjustangles

      OPEN(80,file='anglesALPHAlots.dat20c',status='replace') 
      OPEN(81,file='anglesALPHAlots.res20c',status='replace') 
      OPEN(83,file='anglesALPHAlots.ims20c',status='replace')
      OPEN(86,file='anglesALPHAlots.mag20c',status='replace')

      pi=3.14159265359
      conv=180.0/pi

      idum=-5192-7 

      anph=250.0

      DO 60 jl=1                     !-------cycle over lens parameters, I am using 1 set of parameters
      FLAGa=0
      FLAGk=0      !if reset to 1, that means kappa<0 somewhere

 90   CONTINUE
      xoff=1.0+20.0*(2.0*ran3(idum)-1.0)
      yoff=0.0+20.0*(2.0*ran3(idum)-1.0)
      bA=10.0
      alpA=1.0 + 0.4*(2.0*ran3(idum)-1.0)
      q2A=1.0 - 0.3*ran3(idum)
      s2A=305.0 + 295.0*(2.0*ran3(idum)-1.0)
      K2A=0.0 
      bB=100.0 + 70.0*(2.0*ran3(idum)-1.0)
      alpB=1.0 + 0.4*(2.0*ran3(idum)-1.0)
      q2B=1.0 + 0.3*(2.0*ran3(idum)-1.0)
      s2B=8.0d3 + 7.0d3*(2.0*ran3(idum)-1.0)
      K2B=0.0 
C+ 0.4*(2.0*ran3(idum)-1.0)

      CALL setscale(bA,alpA,q2A,s2A,K2A,bB,alpB,q2B,s2B,K2B,sc,xE)
      bA=bA/sc**(alpA-2.0)
      bB=bb/sc**(alpB-2.0)
      s2A=s2A*sc*sc
      s2B=s2B*sc*sc
      IF(xE.LT.1.0d-3)GO TO 90
      IF(s2B.GT.1.0d+4)GO TO 90
      IF(s2A.GT.s2B)GO TO 90

      kap0A=bA*alpA*s2A**(0.5*alpA-1.0)
      kap0B=bB*alpB*s2B**(0.5*alpB-1.0)
      !WRITE( 6,*)'- - - - ',kap0A,kap0B

C     FOR DRAWING CAUSTICS (divide each pixel into ispm/2, for interpolation):
      sp=2./dfloat(ispm)  
      !anph=dfloat(nph)

C     CALCULATE MASS DISTRIBUTION, DEFLECTION ANGLES, ETC. ON A GRID
      kapmax=0.0
      DO 51 ix=0,np
       DO 52 iy=0,np

        x=dfloat(ix)-anph
        y=dfloat(iy)-anph
        CALL alplens(bA,alpA,q2A,s2A,K2A,x,y,kappaA,gamm1A,gamm2A
     &                                              ,potA,potxA,potyA)
        xx=x-xoff
        yy=y-yoff
        CALL alplens(bB,alpB,q2B,s2B,K2B,xx,yy,kappaB,gamm1B,gamm2B
     &                                              ,potB,potxB,potyB)
        kap(ix,iy)=kappaA+kappaB
        IF(kap(ix,iy).LT.0.0)FLAGk=1
        IF(kap(ix,iy).GT.kapmax)THEN    !find actual pixel of maximum density 
          kapmax=kap(ix,iy)
          ixmax=ix
          iymax=iy
        END IF
        gradx(ix,iy)=potxA+potxB
        grady(ix,iy)=potyA+potyB
        gam1(ix,iy)=gamm1A+gamm1B
        gam2(ix,iy)=gamm2A+gamm2B
        gamma=dsqrt(gam1(ix,iy)*gam1(ix,iy)+gam2(ix,iy)*gam2(ix,iy))
        eigen1(ix,iy)=(1.0-kap(ix,iy)-gamma)
        eigen2(ix,iy)=(1.0-kap(ix,iy)+gamma)

C       lensing potential at (x,y)
        lpot(ix,iy)=potA+potB

 52    CONTINUE
 51   CONTINUE

      WRITE( 6,*)jl,bB/bA,s2B/s2A,'  FLAGk: ',FLAGk
      !WRITE( 6,82)bA,alpA,q2A,s2A,K2A,bB,alpB,q2B,s2B,K2B
      IF(FLAGk.EQ.1)GO TO 90

      !initialize the extreme points of the diamond caustic
      xbmin=100000.
      ybmin=100000.
      xbmax=0.
      ybmax=0.

      DO 41 iy=1,np-1
        DO 42 ix=1,np-1

           IF(eigen1(ix-1,iy)*eigen1(ix+1,iy).LT.0.0             !DIAMOND CAUSTIC
     &       .OR.eigen1(ix,iy-1)*eigen1(ix,iy+1).LT.0.0)THEN
c               use these 3 lines for no-interpolation results
                !aixs=(dfloat(ix)-gradx(ix,iy))
                !aiys=(dfloat(iy)-grady(ix,iy))   

                !interpolate to get better caustics 
                 DO 441 iye=1,ispm
                   yint=dfloat(iy-1)+dfloat(iye)*sp
                   DO 442 ixe=1,ispm
                     xint=dfloat(ix-1)+dfloat(ixe)*sp
                     CALL intereigen1(ix+1,iy+1,xint,yint,ei1)
                     subeigen1(ixe,iye)=ei1
                     CALL intergradx (ix+1,iy+1,xint,yint,grx)
                     subgradx (ixe,iye)=grx
                     CALL intergrady (ix+1,iy+1,xint,yint,gry)
                     subgrady (ixe,iye)=gry
 442               CONTINUE
 441             CONTINUE

                 DO 451 iye=2,ispm-1
                    DO 452 ixe=2,ispm-1
                  IF(subeigen1(ixe-1,iye)*subeigen1(ixe+1,iye).LT.0..OR.            
     &              subeigen1(ixe,iye-1)*subeigen1(ixe,iye+1).LT.0.)THEN
                    aixs=(dfloat(ix-1)+dfloat(ixe)*sp-subgradx(ixe,iye))
                    aiys=(dfloat(iy-1)+dfloat(iye)*sp-subgrady(ixe,iye)) 
                    xbmax=MAX(xbmax,aixs)
                    ybmax=MAX(ybmax,aiys)
                    xbmin=MIN(xbmin,aixs)
                    ybmin=MIN(ybmin,aiys)
                  END IF
 452                CONTINUE
 451             CONTINUE        
           END IF

           IF(eigen2(ix-1,iy)*eigen2(ix+1,iy).LT.0.0             !OVAL CAUSTIC
     &       .OR.eigen2(ix,iy-1)*eigen2(ix,iy+1).LT.0.0)THEN
c               use these 3 lines for no-interpolation results
                !aixs=(dfloat(ix)-gradx(ix,iy))
                !aiys=(dfloat(iy)-grady(ix,iy))   

c                !interpolate to get better caustics
                 DO 461 iye=1,ispm
                   yint=dfloat(iy-1)+dfloat(iye)*sp
                   DO 462 ixe=1,ispm
                     xint=dfloat(ix-1)+dfloat(ixe)*sp
                     CALL intereigen2(ix+1,iy+1,xint,yint,ei2)
                     subeigen2(ixe,iye)=ei2
                     CALL intergradx (ix+1,iy+1,xint,yint,grx)
                     subgradx (ixe,iye)=grx
                     CALL intergrady (ix+1,iy+1,xint,yint,gry)
                     subgrady (ixe,iye)=gry
 462               CONTINUE
 461             CONTINUE

                 DO 471 iye=2,ispm-1
                    DO 472 ixe=2,ispm-1
                IF(subeigen2(ixe-1,iye)*subeigen2(ixe+1,iye).LT.0..OR.            
     &            subeigen2(ixe,iye-1)*subeigen2(ixe,iye+1).LT.0.)THEN
                  aixs=(dfloat(ix-1)+dfloat(ixe)*sp-subgradx(ixe,iye))
                  aiys=(dfloat(iy-1)+dfloat(iye)*sp-subgrady(ixe,iye)) 
                END IF
 472                CONTINUE
 471             CONTINUE
           END IF

 42     CONTINUE
 41   CONTINUE

C     ===========SOURCE POSITION NOT USED EARLIER IN THE CODE==========================

      hiim=0                                     !count sources with more than 5 images
      in=0
      xrange=(xbmax-xbmin)
      xmid=(xbmax+xbmin)/2.
      yrange=(ybmax-ybmin)
      ymid=(ybmax+ybmin)/2.
      !PRINT*,' diamond range ', xbmin,xbmax,ybmin,ybmax
      !PRINT*,' center ',xmid,ymid
      
      slant1= (ybmax-ymid)/(xmid-xbmax)  ! slope is -ve
      slant2= (ybmax-ymid)/(xmid-xbmin)
      slant3= (ybmin-ymid)/(xmid-xbmin)  ! slope is -ve
      slant4= (ybmin-ymid)/(xmid-xbmax)
      !PRINT*, ' slants ',slant1,slant2,slant3,slant4

      !!DO 92 inc=1,inmax
      DO WHILE (in.LE.inmax)
     
 333     CONTINUE
         xs=(ran3(idum)-0.5)*0.6*xrange+xmid 
         ys=(ran3(idum)-0.5)*0.6*yrange+ymid

      jim=0                                      !image number counter for a given source

      angso=datan((ys-anph)/(xs-anph))
      IF((xs-anph).LT.0.0)angso=angso+pi
      IF(angso.LT.0.0)angso=angso+2.*pi
      angso=angso*180./pi

C     FIND IMAGES:
C     ------------

      DO 71 iy=1,np-1
        DO 72 ix=1,np-1
           r2 = (dfloat(ix)-xs)*(dfloat(ix)-xs)  
     &         +(dfloat(iy)-ys)*(dfloat(iy)-ys)
           !SUM UP geom + grav parts of time delay
           llpot(ix,iy)=0.5*r2-lpot(ix,iy)      
           delxx(ix,iy)=(dfloat(ix)-gradx(ix,iy)-xs)
           delyy(ix,iy)=(dfloat(iy)-grady(ix,iy)-ys)
 72     CONTINUE
 71   CONTINUE   

      DO 75 ix=2,np-1
      DO 76 iy=2,np-1

      nminus=0
      IF(delxx(ix-1,iy-1)*delxx(ix  ,iy-1).LT.0)nminus=nminus+1
      IF(delxx(ix  ,iy-1)*delxx(ix  ,iy  ).LT.0)nminus=nminus+1
      IF(delxx(ix  ,iy  )*delxx(ix-1,iy  ).LT.0)nminus=nminus+1
      IF(delxx(ix-1,iy  )*delxx(ix-1,iy-1).LT.0)nminus=nminus+1

      IF(nminus.EQ.2)THEN
      mminus=0
      IF(delyy(ix-1,iy-1)*delyy(ix  ,iy-1).LT.0)mminus=mminus+1
      IF(delyy(ix  ,iy-1)*delyy(ix  ,iy  ).LT.0)mminus=mminus+1
      IF(delyy(ix  ,iy  )*delyy(ix-1,iy  ).LT.0)mminus=mminus+1
      IF(delyy(ix-1,iy  )*delyy(ix-1,iy-1).LT.0)mminus=mminus+1

      IF(mminus.EQ.2)THEN
            
      i=0

      IF (delxx(ix-1,iy-1)*delxx(ix,iy-1).LE.0.0) THEN
       i=i+1
      redxpt(i)=dabs(delxx(ix-1,iy-1)/(delxx(ix-1,iy-1)-delxx(ix,iy-1)))
       redypt(i)=0.0
      END IF

      IF (delxx(ix,iy-1)*delxx(ix,iy).LE.0.0) THEN
       i=i+1
       redxpt(i)=1.0
       redypt(i)=dabs(delxx(ix,iy-1)/(delxx(ix,iy-1)-delxx(ix,iy)))
      END IF

      IF (delxx(ix-1,iy)*delxx(ix,iy).LE.0.0) THEN
       i=i+1
       redxpt(i)=dabs(delxx(ix-1,iy)/(delxx(ix-1,iy)-delxx(ix,iy)))
       redypt(i)=1.0
      END IF
      
      IF (delxx(ix-1,iy-1)*delxx(ix-1,iy).LE.0.0) THEN
       i=i+1
       redxpt(i)=0.0
      redypt(i)=dabs(delxx(ix-1,iy-1)/(delxx(ix-1,iy-1)-delxx(ix-1,iy)))
      END IF

* CHECK
      IF(i.NE.2)PRINT*, ' red - i is not TWO'

      i=0

      IF (delyy(ix-1,iy-1)*delyy(ix,iy-1).LE.0.0) THEN
       i=i+1
      bluxpt(i)=dabs(delyy(ix-1,iy-1)/(delyy(ix-1,iy-1)-delyy(ix,iy-1)))
       bluypt(i)=0.0
      END IF

      IF (delyy(ix,iy-1)*delyy(ix,iy).LE.0.0) THEN
       i=i+1
       bluxpt(i)=1.0
       bluypt(i)=dabs(delyy(ix,iy-1)/(delyy(ix,iy-1)-delyy(ix,iy)))
      END IF

      IF (delyy(ix-1,iy)*delyy(ix,iy).LE.0.0) THEN
       i=i+1
       bluxpt(i)=dabs(delyy(ix-1,iy)/(delyy(ix-1,iy)-delyy(ix,iy)))
       bluypt(i)=1.0
      END IF

      IF (delyy(ix-1,iy-1)*delyy(ix-1,iy).LE.0.0) THEN
       i=i+1
       bluxpt(i)=0.0
      bluypt(i)=dabs(delyy(ix-1,iy-1)/(delyy(ix-1,iy-1)-delyy(ix-1,iy)))
      END IF

* CHECK
      IF(i.NE.2)PRINT*, ' blu - i is not TWO'

* A and B constants of the red and blu lines:

      redA=(redypt(1)-redypt(2))/(redxpt(1)-redxpt(2))
      redB= redypt(1)-redA*redxpt(1)
      bluA=(bluypt(1)-bluypt(2))/(bluxpt(1)-bluxpt(2))
      bluB= bluypt(1)-bluA*bluxpt(1)
      !PRINT*, ' colors: ', redA,redB,bluA,bluB

* INTERSECTION of the red and the blu lines:

      xint=(bluB-redB)/(redA-bluA)
      yint=redA*xint+redB
      !PRINT*, ' intersection point ', xint,yint

* IS THE RED/BLU INTERSECTION POINT WITHIN THE GRID SQUARE?

      IF(xint.GT.0.0.AND.xint.LE.1.0)THEN
         IF(yint.GT.0.0.AND.yint.LE.1.0)THEN     !if YES, then found an image
           xint=dfloat(ix-1)+xint
           yint=dfloat(iy-1)+yint

           x=xint-anph
           y=yint-anph
           CALL alplens(bA,alpA,q2A,s2A,K2A,x,y,kappaA,gamm1A,gamm2A
     &                                              ,potA,potxA,potyA)
           xx=x-xoff
           yy=y-yoff
           CALL alplens(bB,alpB,q2B,s2B,K2B,xx,yy,kappaB,gamm1B,gamm2B
     &                                              ,potB,potxB,potyB)
           kaph=kappaA+kappaB
           gam1h=gamm1A+gamm1B
           gam2h=gamm2A+gamm2B
           magn=1.0/((1.0-kaph)*(1.0-kaph)-gam1h*gam1h-gam2h*gam2h)
C          lensing potential at (x,y)
           r2 = (xint-xs)*(xint-xs) + (yint-ys)*(yint-ys)
           llpo=0.5*r2-(potA+potB)

C          image distances and angles are now w.r.t. highest density pixel
           dist=dsqrt((xint-ixmax)*(xint-ixmax)
     &                    +(yint-iymax)*(yint-iymax))
           angle=datan((yint-iymax)/(xint-ixmax))
           IF((xint-ixmax).LT.0.0)angle=angle+pi
           IF(angle.LT.0.0)angle=angle+2.0*pi
           angle=angle*180.0/pi       !convert from radians to degrees

             jim=jim+1       
             xim(jim)=xint
             yim(jim)=yint
             dst(jim)=dist
             ang(jim)=angle
             amp(jim)=magn
             lpo(jim)=llpo  !used to be: llpot(ix,iy)

         END IF
      END IF

      END IF
      END IF

 76   CONTINUE
 75   CONTINUE

      !WRITE( 6,*)'jim ', jim
      IF(jim.EQ.5)THEN                           !---------------FOUND A QUAD
      in=in+1

C     REORDER IMAGES ACCORDING TO THE CALCULATED ARRIVAL TIME
C     -------------------------------------------------------

      FLAG=0
      DO WHILE (FLAG.EQ.0)        
      DO 65 i=1,5
       DO 66 j=1,5
        IF(lpo(i).GT.lpo(j).AND.i.LT.j)THEN
          CALL flip(i,j)
        END IF
 66    CONTINUE
 65   CONTINUE
          IF(lpo(1).LT.lpo(2)
     &  .AND.lpo(2).LT.lpo(3)
     &  .AND.lpo(3).LT.lpo(4)
     &  .AND.lpo(4).LT.lpo(5))FLAG=1
       END DO

C     SOME FRACTION OF QUADS WILL HAVE MISORDERED IMAGE SEQUENCE; CORRECT THAT
C     ------------------------------------------------------------------------

c         redefine angles with respect to the 1st arriving
           ang1=ang(1)
          ang4=ang(4)-ang1
          ang3=ang(3)-ang1
          ang2=ang(2)-ang1
          IF(ang4.LT.0.0)ang4=ang4+360.
          IF(ang3.LT.0.0)ang3=ang3+360.
          IF(ang2.LT.0.0)ang2=ang2+360.

          IF(ang3.GT.ang4)THEN
            ang2=360.0-ang2
            ang3=360.0-ang3
            ang4=360.0-ang4
          END IF

          ang12=dabs(ang2)
          ang23=dabs(ang2-ang3)
          ang34=dabs(ang3-ang4)

      IF(ang2.LT.ang4)THEN              !im#3 is anti-clockwise from im#1
        bis12=0.5*(ang2)
        bis34=0.5*(ang3+ang4)
      ELSE                              !im#4 is anti-clockwise from im#1
        bis12=0.5*(360.+ang2)
        bis34=0.5*(ang3+ang4)
      END IF
      
      bisd=dabs(bis12-bis34)            !bisector difference
 
      sock=0.
      IF(dst(1).LT.dst(2))sock=sock+1000.
      IF(dst(2).LT.dst(3))sock=sock+100.
      IF(dst(3).LT.dst(4))sock=sock+10.
      IF(dst(1).LT.dst(4))sock=sock+1.

      dst(2)=dst(2)/dst(1)
      dst(3)=dst(3)/dst(1)
      dst(4)=dst(4)/dst(1)

      !WRITE( 6,70)xs,ys,ang12,ang34,ang23,sock,dst(2),dst(3),dst(4)
      WRITE(80,70)xs,ys,ang12,ang34,ang23,sock,dst(2),dst(3),dst(4)
 70   FORMAT(1x,2(f8.3,1x),2x,3(f7.3,1x),2x,f5.0,3(1x,f6.4))
      WRITE(83,84)xim(1),yim(1),xim(2),yim(2)
     &           ,xim(3),yim(3),xim(4),yim(4)
      WRITE(85,*) ang(1),ang(2),ang(3),ang(4)
      WRITE(86,*) amp(1),amp(2),amp(3),amp(4)
      WRITE( 6,*) in, amp(1),amp(2),amp(3),amp(4)
 84   FORMAT(1x,8(1x,f8.3))

      END IF                           !---------------end of FOUND A QUAD

 50   CONTINUE
      !CLOSE(17)
      !CLOSE(18)
      !CLOSE(26)
      !CLOSE(23)

      END DO

      WRITE(6,*) 'number of sources with more than 5 images', hiim

      STOP
      END
C***********************************************************************
      SUBROUTINE adjustangles()
      IMPLICIT REAL*8 (a-z) 
      INCLUDE 'commonALPHA.file'

      DO 61 i=1,4
        IF(ang(i).GT.360.0)ang(i)=ang(i)-360.0
        IF(ang(i).LT.0.0)ang(i)=ang(i)+360.0
 61   CONTINUE

      RETURN
      END
C***********************************************************************
      SUBROUTINE flip (i,j) 
C     used when reordering the images (after arrival time surface ordering
C     procedure failed); flips images currently numbered as i & j --> j & i
      IMPLICIT REAL*8 (a-z) 
      INCLUDE 'commonALPHA.file'

         slpo=lpo(i)
         sxim=xim(i)
         syim=yim(i)
         sdst=dst(i)
         sang=ang(i)
         samp=amp(i)

         lpo(i)=lpo(j)
         xim(i)=xim(j)
         yim(i)=yim(j)
         dst(i)=dst(j)
         ang(i)=ang(j)
         amp(i)=amp(j)

         lpo(j)=slpo
         xim(j)=sxim
         yim(j)=syim
         dst(j)=sdst
         ang(j)=sang
         amp(j)=samp

      RETURN
      END
C***********************************************************************
      SUBROUTINE intereigen1 (ix,iy,xint,yint,ei1)
C     used for drawing caustics; linearly interpolate the 1st eigenvalue 
C     function using 4 pixels, in a cross configuration around the pixel 
C     containing the image
      IMPLICIT REAL*8 (a-z) 
      INCLUDE 'commonALPHA.file'

      x1=dfloat(ix-2)
      x2=dfloat(ix)
      y1=dfloat(iy-2)
      y2=dfloat(iy)
      t=(xint-x1)/(x2-x1)
      u=(yint-y1)/(y2-y1)

      ei1=(1.-t)*(1.-u)*eigen1(ix-2,iy-2)
     &        +t*(1.-u)*eigen1(ix,  iy-2)
     &             +t*u*eigen1(ix,  iy)
     &        +(1.-t)*u*eigen1(ix-2,iy)

      RETURN
      END
C***********************************************************************
      SUBROUTINE intereigen2 (ix,iy,xint,yint,ei2)
C     used for drawing caustics; linearly interpolate the 2nd eigenvalue 
C     function using 4 pixels, in a cross configuration around the pixel 
C     containing the image
      IMPLICIT REAL*8 (a-z) 
      INCLUDE 'commonALPHA.file'

      x1=dfloat(ix-2)
      x2=dfloat(ix)
      y1=dfloat(iy-2)
      y2=dfloat(iy)
      t=(xint-x1)/(x2-x1)
      u=(yint-y1)/(y2-y1)

      ei2=(1.-t)*(1.-u)*eigen2(ix-2,iy-2)
     &        +t*(1.-u)*eigen2(ix,  iy-2)
     &             +t*u*eigen2(ix,  iy)
     &        +(1.-t)*u*eigen2(ix-2,iy)

      RETURN
      END
C***********************************************************************
      SUBROUTINE intergradx (ix,iy,xint,yint,grx)
C     linearly interpolate the x gradient of lensing potential, using
C     4 pixels, in a cross configuration around the pixel containing the image
      IMPLICIT REAL*8 (a-z) 
      INCLUDE 'commonALPHA.file'

      x1=dfloat(ix-2)
      x2=dfloat(ix)
      y1=dfloat(iy-2)
      y2=dfloat(iy)
      t=(xint-x1)/(x2-x1)
      u=(yint-y1)/(y2-y1)

      grx=(1.-t)*(1.-u)*gradx(ix-2,iy-2)
     &        +t*(1.-u)*gradx(ix,  iy-2)
     &             +t*u*gradx(ix,  iy)
     &        +(1.-t)*u*gradx(ix-2,iy)

      RETURN
      END
C***********************************************************************
      SUBROUTINE intergrady (ix,iy,xint,yint,gry)
C     linearly interpolate the y gradient of lensing potential, using
C     4 pixels, in a cross configuration around the pixel containing the image
      IMPLICIT REAL*8 (a-z) 
      INCLUDE 'commonALPHA.file'

      x1=dfloat(ix-2)
      x2=dfloat(ix)
      y1=dfloat(iy-2)
      y2=dfloat(iy)
      t=(xint-x1)/(x2-x1)
      u=(yint-y1)/(y2-y1)

      gry=(1.-t)*(1.-u)*grady(ix-2,iy-2)
     &        +t*(1.-u)*grady(ix,  iy-2)
     &             +t*u*grady(ix,  iy)
     &        +(1.-t)*u*grady(ix-2,iy)

      RETURN
      END

C=======================================================================
      SUBROUTINE alplens(b,alp,q2,s2,K2,x,y,kappa,gamm1,gamm2
     &                                              ,pot,potx,poty)
C
C  calculate first and second derivatives of the lensing potential
C
      IMPLICIT REAL*8(a-z)

      quan=(s2+x*x+y*y/q2+K2*x*y)

      pot=b*quan**(0.5*alp)

      potx=0.5*b*alp*(2.0*x+K2*y)*quan**(0.5*alp-1.0)
      poty=0.5*b*alp*(2.0*y/q2+K2*x)*quan**(0.5*alp-1.0)

      potxx=b*alp*quan**(0.5*alp-1.0)
     &   +0.5*b*alp*(0.5*alp-1.0)*(2.0*x+K2*y)**2*quan**(0.5*alp-2.0)

      potyy=(b*alp/q2)*quan**(0.5*alp-1.0)
     &   +0.5*b*alp*(0.5*alp-1.0)*(2.0*y/q2+K2*x)**2*quan**(0.5*alp-2.0) 

      potxy=0.5*K2*b*alp*quan**(0.5*alp-1.0)
     &   +0.5*b*alp*(0.5*alp-1.0)*(2.0*y/q2+K2*x)*(2.0*x+K2*y)
     &                                            *quan**(0.5*alp-2.0)

      kappa=0.5*(potxx+potyy)
      gamm1=0.5*(potxx-potyy)
      gamm2=potxy
      gamma=dsqrt(gamm1*gamm1+gamm2*gamm2)
      magn=1.0/((1.0-kappa)*(1.0-kappa)-gamm1*gamm1-gamm2*gamm2)

      RETURN
      END
C=======================================================================
      SUBROUTINE setscale(bA,alpA,q2A,s2A,K2A,bB,alpB,q2B,s2B,K2B,sc,xE)
      IMPLICIT REAL*8(a-z)
      INTEGER itot
      PARAMETER (itot=200)
      INCLUDE 'commonALPHA.file'

      xE=0.0

      rlo=0.01
      rhi=anph
      dlr=dlog10(rhi/rlo)/dfloat(itot)

      y=0.0
      menc=0.0
      DO 21 i=1,itot
        x=rlo*10.0**(dfloat(i)*dlr)
        CALL alplens(bA,alpA,q2A,s2A,K2A,x,y,kappaA,gamm1A,gamm2A
     &                                              ,pot,potx,poty)
        CALL alplens(bB,alpB,q2B,s2B,K2B,x,y,kappaB,gamm1B,gamm2B
     &                                              ,pot,potx,poty)
        kappaAB=kappaA+kappaB
        xbef=rlo*10.0**(dfloat(i-1)*dlr)
        xaft=rlo*10.0**(dfloat(i+1)*dlr)
        dr=0.5*(xaft-xbef)
        menc=menc+2.0*pi*x*kappaAB*dr
        kapave=menc/(pi*x*x)
        IF(kapave.GT.1.0)THEN
          xE=x
          kapaveE=kapave
        END IF
 21   CONTINUE
      sc=0.8*anph/xE

      RETURN
      END
C**********************************************************************
      FUNCTION ran3(idum)
C Function generates random numbers every time it is called, with seed idum1
C in the main code.
C------------------------------------------------------------------------------

       INTEGER idum
c       INTEGER MBIG, MSEED, MZ
       REAL*8 MBIG, MSEED, MZ
       REAL*8 ran3, FAC
c       PARAMETER (MBIG=1000000000, MSEED=161803398, MZ=0, FAC=1./MBIG)
       PARAMETER (MBIG=4000000., MSEED=1618033., MZ=0., FAC=1./MBIG)
       INTEGER i, iff, ii, inext, inextp, k
c       INTEGER mj, mk, ma(55)
       REAL*8 mj, mk, ma(55)
       SAVE iff, inext, inextp, ma
       DATA iff /0/
       if(idum.lt.0.or.iff.eq.0)then
        iff=1
        mj=MSEED-iabs(idum)
        mj=mod(mj, MBIG)
        ma(55)=mj
        mk=1
       do 11 i=1, 54
        ii=mod(21*i, 55)
        ma(ii)=mk
        mk=mj-mk
        if(mk.lt.MZ)mk=mk+MBIG
        mj=ma(ii)
 11     continue
       do 13 k=1, 4
        do 12 i=1, 55
         ma(i)=ma(i)-ma(1+mod(i+30, 55))
         if(ma(i).lt.MZ)ma(i)=ma(i)+MBIG
 12      continue
 13     continue
        inext=0
        inextp=31
        idum=1
       endif
       inext=inext+1
       if(inext.eq.56)inext=1
       inextp=inextp+1
       if(inextp.eq.56)inextp=1
       mj=ma(inext)-ma(inextp)
       if(mj.lt.MZ)mj=mj+MBIG
       ma(inext)=mj
       ran3=mj*FAC
       return
       END
